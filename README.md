# Stack (Call Stack)

```js
function foo(b) {
  let a = 10;
  return a + b + 11;
}

function bar(x) {
  let y = 3;
  return foo(x * y);
}

const baz = bar(7); // assigns 42 to baz
```

# Queue (Event Loop)

In setTimeout function, the second argument indicates a minimum time—not a guaranteed time.

```js
while (queue.waitForMessage()) {
  queue.processNextMessage();
}
```
